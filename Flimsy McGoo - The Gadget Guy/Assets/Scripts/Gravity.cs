﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gravity : MonoBehaviour {

	public Rigidbody2D rigidi;
	public float grav;
	public float speed;
	public float jump;
	public GameObject rairai;
	public Ray ray;
	private SpriteRenderer playerRender;
	bool up;

	// Use this for initialization
	void Start () {
		
		up = false;
		playerRender = GetComponent<SpriteRenderer> ();
		rigidi.gravityScale = grav;

		rairai = GameObject.Find("Ray");
		ray = rairai.GetComponent <Ray> ();
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		if (Input.GetKeyDown (KeyCode.G)) {
			rigidi.gravityScale *= -1;

			if (rigidi.gravityScale < 0) {
				up = true;
				playerRender.flipY = true;

			} else if (rigidi.gravityScale > 0) {
				up = false;
				playerRender.flipY = false;
			}
		}

		if (Input.GetAxis ("Horizontal") < 0) {
			rigidi.velocity = new Vector3 (-speed, rigidi.velocity.y, 0);
		}

		if (Input.GetAxis ("Horizontal") > 0) {
			rigidi.velocity = new Vector3 (speed, rigidi.velocity.y, 0);
		}

		if (Input.GetKeyDown (KeyCode.Space) && ray.GetJump () == true) {
			if (up == false) {
				rigidi.velocity = new Vector3 (rigidi.velocity.x, jump, 0);
			} else if (up == true) {
				rigidi.velocity = new Vector3 (rigidi.velocity.x, -jump, 0);
			}
		}
	}

	public bool GetUp () {
		return up;
	}
}
