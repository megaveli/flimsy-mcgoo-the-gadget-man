﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ray : MonoBehaviour {

	RaycastHit2D hit;
	private float distance;
	public Rigidbody2D player;
	bool canJump;
	public GameObject graavi;
	public Gravity gravity;

	// Use this for initialization
	void Start () {
		graavi = GameObject.Find ("Flimsy McGoover");
		gravity = graavi.GetComponent <Gravity> ();
	}
	
	// Update is called once per frame
	void Update () {

		if (gravity.GetUp() == false) {
			Vector2 down = transform.TransformDirection (Vector2.down);
			hit = Physics2D.Raycast (transform.position, -Vector2.up);
			Debug.DrawRay (transform.position, down, Color.cyan);
			if (hit.collider != null) {
				distance = Mathf.Abs (hit.point.y - transform.position.y);
				print (distance + " " + hit.collider.name);
			}
			if (distance <= 0.71) {
				canJump = true;
			} else {
				canJump = false;
			}
		}

		if (gravity.GetUp() == true) {
			Vector2 up = transform.TransformDirection (Vector2.up);
			hit = Physics2D.Raycast (transform.position, -Vector2.down);
			Debug.DrawRay (transform.position, up, Color.cyan);
			if (hit.collider != null) {
				distance = Mathf.Abs (hit.point.y - transform.position.y);
				print (distance + " " + hit.collider.name);
			}
			if (distance <= 0.71) {
				canJump = true;
			} else {
				canJump = false;
			}
		}
	}

	public bool GetJump () {
		return canJump;
	}
}
