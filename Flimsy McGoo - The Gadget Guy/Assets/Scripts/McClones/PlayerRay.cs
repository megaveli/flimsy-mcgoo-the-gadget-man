﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRay : MonoBehaviour {

	RaycastHit2D hit;
	private float distance;
	public Rigidbody2D player;
	bool canJump;

	void Start () {
		
	}

	void Update () {

		Vector2 down = transform.TransformDirection (Vector2.down);
		hit = Physics2D.Raycast (transform.position, -Vector2.up);
		Debug.DrawRay (transform.position, down, Color.cyan);
		if (hit.collider != null) {
			distance = Mathf.Abs (hit.point.y - transform.position.y);
			//print (distance + " " + hit.collider.name);
		}
		if (distance <= 1.22) {
			canJump = true;
		} else {
			canJump = false;
		}
	}

	public bool GetJump () {
		return canJump;
	}
}
