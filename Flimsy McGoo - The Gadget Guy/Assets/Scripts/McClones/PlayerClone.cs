﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerClone : MonoBehaviour {

	public Transform clonePosRight;
	public Transform clonePosLeft;

	public GameObject clonePrefab;

	private PlayerController playerController;
	private GameObject plyCon;

	void Start () {
	
		plyCon = GameObject.Find("flimsy");
		playerController = plyCon.GetComponent <PlayerController> ();

	}
	
	void Update () {

		if (Input.GetKeyDown (KeyCode.X)) {

			if (playerController.GetTurnRight () == true) {
				GameObject clone;
				clone = (Instantiate (clonePrefab, clonePosRight.position, Quaternion.identity));
			} else {
				GameObject clone;
				clone = (Instantiate (clonePrefab, clonePosLeft.position, Quaternion.identity));
			}
		}
	}
}
