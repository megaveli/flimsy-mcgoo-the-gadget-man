﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	public float speed;
	public float jump;

	private Rigidbody2D playerRig;
	private SpriteRenderer playerRender;
	private Animator playerAnimator;

	private GameObject ray;
	private PlayerRay playerRay;

	private bool turnedRight;

	void Start () {

		playerRig = GetComponent<Rigidbody2D> ();
		playerRender = GetComponent<SpriteRenderer> ();
		playerAnimator = GetComponent<Animator> ();

		ray = GameObject.Find("rayCast");
		playerRay = ray.GetComponent <PlayerRay> ();

		turnedRight = true;
	}

	void FixedUpdate () {

		if (Input.GetAxis ("Horizontal") < 0) {
			playerRig.velocity = new Vector3 (-speed, playerRig.velocity.y, 0);
			playerAnimator.SetBool ("isWalking", true);

			if (turnedRight == true) {
				turnedRight = false;
				playerRender.flipX = true;
			}
		}

		if (Input.GetAxis ("Horizontal") > 0) {
			playerRig.velocity = new Vector3 (speed, playerRig.velocity.y, 0);
			playerAnimator.SetBool ("isWalking", true);

			if (turnedRight == false) {
				turnedRight = true;
				playerRender.flipX = false;
			}
		}
		if (Input.GetAxis ("Horizontal") == 0 && playerRay.GetJump () == true) {
			playerAnimator.SetBool ("isWalking", false);
			playerRig.velocity = new Vector3 (0, playerRig.velocity.y, 0);
		}

		if (Input.GetKeyDown (KeyCode.Z) && playerRay.GetJump () == true) {
			playerRig.velocity = new Vector3 (playerRig.velocity.x, jump, 0);
		}
		//print (playerRig.velocity.x + "  " + playerRig.velocity.y);
	}

	public bool GetTurnRight() {
		return turnedRight;
	}
}
